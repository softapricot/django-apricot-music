""" Django Apricot Core views module
"""
from dacommon.views import BaseView
from dacommon.integrations.bases import BaseAppView
from damusic.integrations.spotify import SpotifyIntegration, Scopes


class ExampleView(BaseView):
    """View for example"""

    template_name = "example.html"


class AppExampleView(BaseAppView):
    """View for grid example"""

    template_name = "example_app.html"
    integration_class = SpotifyIntegration
    integration_scopes = Scopes

    def response_data(self, request):
        self.add_data("app_data", "")
