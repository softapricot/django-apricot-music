""" Demo URLs module
"""
from django.urls import path
from django.contrib import admin
from django.urls.conf import include

from django.conf import settings
from django.conf.urls.static import static
from tests.app.views import ExampleView, AppExampleView


urlpatterns = [
    path("", ExampleView.as_view(), name="home"),
    path("app/", AppExampleView.as_view(), name="app"),
    path("admin/", admin.site.urls),
    path("music/", include("damusic.urls")),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
