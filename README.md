# Django Apricot Fitness : v2023.0.0

Integrations and utilities for music applications

## Development

To prepare develoment environment use poetry

```console
$ pip install poetry==1.4.2
$ poetry install
$ poetry self add poetry-bumpversion
```

To update version across all references use the following command:

```console
$ poetry version <bump rule>
```

### Available bump rules:

* patch
* minor
* major
* prepatch
* preminor
* premajor
* prerelease

## Running tests and validations

```console
$ poetry run tox -e <testenv name>
```

Testenvs

* pylint
* flake8
* build
* coverage
