""" Django Apricot accounts tags module
"""
from django import template
from django.contrib.auth.models import AnonymousUser
from damusic.integrations.spotify import (
    SpotifyIntegration,
    Scopes,
    TimeRange,
    ItemType,
)

register = template.Library()


@register.inclusion_tag("dapricot/music/tags/badge.html", takes_context=True)
def spotify_badge(context, user=None):
    """Returns user badge"""
    request = context["request"]
    data = {
        "api_authenticated": False,
    }
    if user is None:
        user = request.user

    if not isinstance(user, AnonymousUser):
        spotify = SpotifyIntegration(
            request, [Scopes.USER_READ_EMAIL, Scopes.USER_READ_PRIVATE], user=user
        )

        if spotify.is_authenticated:
            profile = spotify.user
            data["profile"] = (
                profile["display_name"],
                profile["uri"],
                profile["images"],
            )
            data["api_authenticated"] = spotify.is_authenticated

    return data


@register.inclusion_tag(
    "dapricot/music/tags/currently_playing.html", takes_context=True
)
def spotify_currently_playing(context, user=None):
    """Returns user currently playing"""
    request = context["request"]
    data = {
        "api_authenticated": False,
    }
    if user is None:
        user = request.user

    if not isinstance(user, AnonymousUser):
        spotify = SpotifyIntegration(
            request,
            [
                Scopes.USER_READ_CURRENTLY_PLAYING,
                Scopes.USER_READ_PLAYBACK_STATE,
            ],
            user=user,
        )

        if spotify.is_authenticated:
            currently_playing = spotify.currently_playing

            if currently_playing is not None:
                if currently_playing["item"]["type"] == "episode":
                    data["currently_playing"] = {
                        "episode": [
                            currently_playing["item"]["name"],
                            currently_playing["item"]["uri"],
                        ],
                        "show": [
                            currently_playing["item"]["show"]["name"],
                            currently_playing["item"]["show"]["uri"],
                            currently_playing["item"]["show"]["images"][0]["url"],
                        ],
                    }
                elif currently_playing["item"]["type"] == "track":
                    data["currently_playing"] = {
                        "track": [
                            currently_playing["item"]["name"],
                            currently_playing["item"]["uri"],
                        ],
                        "artist": [
                            (item["name"], item["uri"])
                            for item in currently_playing["item"]["artists"]
                        ],
                        "album": [
                            currently_playing["item"]["album"]["name"],
                            currently_playing["item"]["album"]["uri"],
                            currently_playing["item"]["album"]["images"][0]["url"],
                        ],
                    }

            data["api_authenticated"] = spotify.is_authenticated

    return data


@register.inclusion_tag("dapricot/music/tags/top_artists.html", takes_context=True)
def spotify_top_artists(
    context,
    time_range: TimeRange = TimeRange.SHORT_TERM,
    limit: int = 10,
    offset: int = 0,
    user=None,
):
    """Returns user top artists"""
    request = context["request"]
    data = {
        "api_authenticated": False,
    }
    if user is None:
        user = request.user

    if not isinstance(user, AnonymousUser):
        spotify = SpotifyIntegration(
            request,
            [
                Scopes.USER_TOP_READ,
            ],
            user=user,
        )

        if spotify.is_authenticated:
            top_items = spotify.get_user_top_items(
                time_range.value, limit, offset, ItemType.ARTIST
            )
            data["items"] = []

            for item in top_items["items"]:
                data["items"].append(
                    (
                        item["name"],
                        item["uri"],
                        item["images"][0]["url"],
                    )
                )

            data["api_authenticated"] = spotify.is_authenticated

    return data


@register.inclusion_tag("dapricot/music/tags/top_songs.html", takes_context=True)
def spotify_top_songs(
    context,
    time_range: TimeRange = TimeRange.SHORT_TERM,
    limit: int = 10,
    offset: int = 0,
    user=None,
):
    """Returns user top songs"""
    request = context["request"]
    data = {
        "api_authenticated": False,
    }
    if user is None:
        user = request.user

    if not isinstance(user, AnonymousUser):
        spotify = SpotifyIntegration(
            request,
            [
                Scopes.USER_TOP_READ,
            ],
            user=user,
        )

        if spotify.is_authenticated:
            top_items = spotify.get_user_top_items(
                time_range.value, limit, offset, ItemType.TRACK
            )
            data["items"] = []

            for item in top_items["items"]:
                data["items"].append(
                    (
                        item["name"],
                        ",".join(artist["name"] for artist in item["artists"]),
                        item["uri"],
                        item["album"]["images"][0]["url"],
                    )
                )

            data["api_authenticated"] = spotify.is_authenticated

    return data
