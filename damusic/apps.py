""" Django Apricot app module
"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

from dacommon.utils import setup_default_settings
from damusic import app_settings


class DjangoApricotConfig(AppConfig):
    """App's config"""

    default_auto_field = "django.db.models.AutoField"
    name = "damusic"
    label = "damusic"
    verbose_name = _("Django Apricot Music")

    def ready(self):
        setup_default_settings(app_settings)
