""" Views module
"""
from dacommon.integrations.bases import BaseAuthView
from damusic.integrations.spotify import SpotifyIntegration, Scopes


class SpotifyAuthView(BaseAuthView):
    """Strava wuthentication view"""

    template_name = "dapricot/music/spotify_auth.html"
    integration_class = SpotifyIntegration
    integration_scopes = Scopes
