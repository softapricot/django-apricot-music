""" Models module
"""
from django.db import models
from django.contrib.auth import get_user_model

from dacommon.models import ProfileModelMixin, IntegrationDataOrigin


class DataOrigin(models.IntegerChoices):
    """Integrations data origin"""

    SPOTIFY = IntegrationDataOrigin.SPOTIFY


class MusicProfile(models.Model, ProfileModelMixin):
    """Fitness Profile model"""

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    origin = models.CharField(
        choices=DataOrigin.choices, max_length=1, default=DataOrigin.SPOTIFY
    )

    class Meta:
        verbose_name = "music profile"
        verbose_name_plural = "music_profiles"
        unique_together = (
            "origin",
            "user",
        )

    def __str__(self):
        return f"{self.user}_{self.origin}"
