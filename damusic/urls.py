""" URLs module
"""
from django.urls.conf import path
from damusic import apps
from damusic.views import SpotifyAuthView


app_name = apps.DjangoApricotConfig.label

urlpatterns = [
    path("auth", SpotifyAuthView.as_view(), name="auth"),
    path("auth/f", SpotifyAuthView.as_view(), {"force_auth": True}, name="auth_force"),
]
