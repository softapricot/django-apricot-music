""" Spotify integration module
"""
import base64
from enum import Enum
from urllib.parse import urlencode
from datetime import timedelta, datetime

from django.urls import reverse
from django.shortcuts import redirect
from django.conf import settings

from dacommon.integrations.bases import BaseIntegration
from dacommon.utils.generators import get_random_string
from damusic.models import DataOrigin, MusicProfile


class Scopes(Enum):
    """Authorization scopes"""

    USER_READ_PRIVATE = "user-read-private"
    USER_READ_EMAIL = "user-read-email"
    USER_READ_CURRENTLY_PLAYING = "user-read-currently-playing"
    USER_READ_PLAYBACK_STATE = "user-read-playback-state"
    USER_TOP_READ = "user-top-read"


class TimeRange(Enum):
    """Time ranges"""

    SHORT_TERM = "short_term"
    MEDIUM_TERM = "medium_term"
    LONG_TERM = "long_term"


class ItemType(Enum):
    """Time ranges"""

    ARTIST = "artist"
    TRACK = "track"


class SpotifyIntegration(BaseIntegration):
    """Integration class for Spotify API"""

    origin = DataOrigin.SPOTIFY
    base_uri = "https://api.spotify.com/v1/"
    datetime_format = "%Y-%m-%dT%H:%M:%SZ"
    profile_model = MusicProfile

    def __init__(self, context, scopes: list, user=None):
        self._client_id = settings.SPOTIFY_CLIENT_ID
        self._client_secret = settings.SPOTIFY_CLIENT_SECRET
        self.scopes = ",".join(
            scope if isinstance(scope, str) else scope.value for scope in scopes
        )
        super().__init__(context, user)

    def authenticate(self, force=False):
        """Redirection object for integration authentication"""
        state = get_random_string(16, punctuation=False)

        self._context.session["spotify_state"] = state

        url = "https://accounts.spotify.com/authorize?" + urlencode(
            {
                "response_type": "code",
                "client_id": self._client_id,
                "redirect_uri": f"http://{self._context.get_host()}{reverse('damusic:auth')}",
                "state": state,
                "scope": self.scopes,
                "show_dialog": force,
            }
        )

        return redirect(url)

    def generate_auth_token(self, only_refresh: bool = False):
        payload = None
        refresh_token = self.profile.get_token("refresh_token")
        message = f"{self._client_id}:{self._client_secret}".encode("ascii")
        message = base64.b64encode(message).decode("ascii")
        headers = {"Authorization": f"Basic {message}"}

        if refresh_token is not None and refresh_token.is_expired:
            payload = {
                "grant_type": "refresh_token",
                "refresh_token": refresh_token.token,
            }

        elif self._context.GET.get("code", None) and not only_refresh:
            if (
                self._context.GET.get("state", "")
                == self._context.session["spotify_state"]
            ):
                payload = {
                    "code": self._context.GET.get("code"),
                    "redirect_uri": f"http://{self._context.get_host()}{reverse('damusic:auth')}",
                    "grant_type": "authorization_code",
                }
            # redirect error state code missmatch

        if payload is not None:
            response = self.request(
                "https://accounts.spotify.com/api/token",
                SpotifyIntegration.RequestAction.POST,
                payload=payload,
                headers=headers,
            )
            tokens = response.json()

            if self.single_use:
                setattr(self, "access_token", tokens["access_token"])

            else:
                expires_at = datetime.now() + timedelta(seconds=tokens["expires_in"])
                self.profile.create_or_update_token(
                    "access_token", tokens["access_token"], expires_at.timestamp()
                )
                if "refresh_token" in tokens:
                    self.profile.create_or_update_token(
                        "refresh_token", tokens["refresh_token"], expires_at.timestamp()
                    )

    @property
    def user(self):
        """Returns user data"""
        response = self.request(
            "me",
            SpotifyIntegration.RequestAction.GET,
            auth_headers=self.auth_headers,
        )
        return response.json()

    @property
    def currently_playing(self):
        """Returns user data"""
        response = self.request(
            "me/player/currently-playing",
            SpotifyIntegration.RequestAction.GET,
            auth_headers=self.auth_headers,
            use_cache=True,
            cache_expires_after=timedelta(minutes=2),
            query={
                "additional_types": "track,episode",
            },
        )

        if response.status_code == 200:
            return response.json()

        return None

    def get_user_top_items(self, time_range, limit, offset, item_type: ItemType):
        """Returns user top items"""

        response = self.request(
            f"me/top/{item_type.value}s",
            SpotifyIntegration.RequestAction.GET,
            auth_headers=self.auth_headers,
            use_cache=True,
            query={"time_range": time_range, "limit": limit, "offset": offset},
        )

        if response.status_code == 200:
            return response.json()

        return None
